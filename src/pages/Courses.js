import CourseCard from "../components/CourseCard";
import { useEffect, useState } from "react";



export default function Courses(){

    // The "map" method loops through the individual course objects in our array and returns a component for each course
    // Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
    // Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the courseProp

    const [ courses, setCourses ] = useState([]);
    
    // Retrieves the courses from the database upon initial render of the "Courses" component
    useEffect(() => {
        fetch(`http://localhost:4000/courses/`).then(res => res.json()).then(data => {
            console.log(data)

            setCourses(data.map(course => {
                return (
                    <CourseCard key={ course._id } course={course} />
                )
            }))

        })
    }, []);



    return(
        <>
            { courses }
        </>
    )
}
