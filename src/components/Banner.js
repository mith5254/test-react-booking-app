import { Button, Col, Row } from "react-bootstrap";

export default function Banner(status){
    if(status.notFound){
        return (
            <Row>
                <Col className="p-5">
                <h1>Page Not Found</h1>
                <p>Go back to the <a href="/">homepage</a></p>
                </Col>
            </Row>
        )
    };

    return (
        <Row>
            <Col className="p-5">
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere.</p>
                <Button variant="primary">Enroll Now!</Button>
            </Col>
        </Row>
    )
    
}