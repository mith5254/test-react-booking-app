import { useEffect, useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function CourseCard({ course }) {
    // console.log(course)
    // console.log(typeof course)

    const { _id, name, description, price } = course;

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax
         // const [getter, setter] = useState(initialGetterValue);
    
    // const [ count, setCount ] = useState(0); 
    // const [ seats, setSeat] = useState(30);
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first 
    
    // console.log(useState(0));

    // function enroll(){
    //     if(seats > 0){
    //         setCount(count + 1);
    //         setSeat(seats - 1);
    //     }
        
    //     // console.log(`Enrollees : ${count}`);
    // }

    // useEffect(() => {
    //     if(seats === 0){
    //         alert("No more seats")
    //     };
    // }, [seats])


    return (
        <Card className='mt-3'>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle >Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                <Link className='btn btn-primary' to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
